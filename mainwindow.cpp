﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qevent.h>
#include<QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{

    if(ui->myHalconWin->IsDrawing())
    {
        QMessageBox::information(NULL,QStringLiteral("提示"),QStringLiteral("图像区域，右键结束绘图"),QMessageBox::Yes);
        event->ignore();
    }
}
