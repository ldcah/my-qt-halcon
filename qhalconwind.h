﻿#ifndef MyViewCtrl_H
#define MyViewCtrl_H

#include <QWidget>
#include<QMetaEnum>
#include<QEvent>
#include<QDebug>

#include "HalconCpp.h"
using namespace HalconCpp;

namespace Ui {
class QHalconWind;
}

class QHalconWind : public QWidget
{
    Q_OBJECT

public:
    explicit QHalconWind(QWidget *parent = nullptr);
    ~QHalconWind();

    void calWH();

    void GetPartFloat(double *row1, double *col1, double *row2, double *col2);


    bool IsDrawing(){return mIsDraw;}


protected :
    void resizeEvent(QResizeEvent*);
    void wheelEvent(QWheelEvent *event);



    void LabelMouseMoveEvent();
    bool eventFilter(QObject *obj, QEvent *event);


signals:
    void xy_grays(int x,int y,int* grays);


private slots:

    void on_tb1Pic_clicked();

    void on_tb2Reset_clicked();

    void on_tb3Circle_clicked();

    void on_tb4Rect1_clicked();

    void on_tb5Rect2_clicked();

private:
    Ui::QHalconWind *ui;


    //Halcon窗口的参数
    HTuple htWindowHandle,htWidth,htHeight;
    HTuple htWindWidth,htWindHeight;
    HObject hoImage;

    HTuple htMouseRow,htMouseCol,htButton;//获取halcon 窗体上的鼠标点与
    HTuple last_hvMouseRow,last_hvMouseCol,last_hvButton;

    HTuple move_hvMouseRow,move_hvMouseCol;//计算移动值

    HTuple startRowBf, startColBf, endRowBf, endColBf;


    QPoint lastMousePos;
    double lastRow1, lastCol1, lastRow2, lastCol2;

    bool m_bIsMove,mIsDraw;                //是否移动图像标识


};



#endif // MyViewCtrl_H
